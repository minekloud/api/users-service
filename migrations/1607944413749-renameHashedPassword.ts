import { MigrationInterface, QueryRunner } from 'typeorm';

export class renameHashedPassword1607944413749 implements MigrationInterface {
	name = 'renameHashedPassword1607944413749';

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`
            ALTER TABLE "credentials"
                RENAME COLUMN "password" TO "hashedPassword"
        `);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`
            ALTER TABLE "credentials"
                RENAME COLUMN "hashedPassword" TO "password"
        `);
	}
}
