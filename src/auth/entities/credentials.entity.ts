import { User } from '../../users/entities/user.entity';
import {
	BeforeInsert,
	Column,
	Entity,
	JoinColumn,
	OneToOne,
	PrimaryColumn,
} from 'typeorm';

@Entity()
export class Credentials {
	@PrimaryColumn()
	id: number;

	@OneToOne(() => User, { nullable: false })
	@JoinColumn()
	user: User;

	@Column()
	hashedPassword: string;

	@BeforeInsert()
	updateDates() {
		this.id = this.user.id;
	}
}
