import {
	BadRequestException,
	Body,
	Controller,
	Get,
	Post,
	Request,
	ServiceUnavailableException,
	UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { User } from '../users/entities/user.entity';
import { UsersService } from '../users/users.service';
import { DeepPartial } from 'typeorm';
import { AuthService } from './auth.service';
import { RegisterDto } from './dto/register.dto';
import { LoginDto } from './dto/login.dto';
import {
	ApiBadRequestResponse,
	ApiBearerAuth,
	ApiBody,
	ApiTags,
	ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { JwtAuthGuard } from './jwt-auth.guard';
import { UnleashService } from 'src/unleash/unleash.service';
import { FeatureFlags } from 'src/unleash/featureFlags.enum';
import { FeatureEnabledGuard } from 'src/unleash/feature-enabled.guard';
import { FeatureDisabledGuard } from 'src/unleash/feature-disabled.guard';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
	constructor(
		private readonly authService: AuthService,
		private readonly usersService: UsersService,
	) {}

	@UseGuards(
		FeatureDisabledGuard(FeatureFlags.DISABLE_LOGIN),
		AuthGuard('local'),
	)
	@Post('login')
	@ApiBody({ type: LoginDto })
	@ApiUnauthorizedResponse()
	async login(@Request() req) {
		return this.authService.login(req.user);
	}

	@UseGuards(FeatureDisabledGuard(FeatureFlags.DISABLE_REGISTRATION))
	@Post('register')
	@ApiBadRequestResponse()
	async register(@Body() registerDto: RegisterDto) {
		const errors = [];
		if (await this.usersService.findOneByUsername(registerDto.username)) {
			errors.push({
				field: 'username',
				message: 'This username is already taken.',
			});
		}

		if (await this.usersService.findOneByEmail(registerDto.email)) {
			errors.push({
				field: 'email',
				message: 'This email address is already taken.',
			});
		}

		if (errors.length > 0) {
			throw new BadRequestException(errors);
		}

		const user: DeepPartial<User> = {
			username: registerDto.username,
			email: registerDto.email,
		};

		return this.authService.register(user, registerDto.password);
	}

	// This is used by Traefik for forward auth
	@ApiBearerAuth()
	@UseGuards(JwtAuthGuard)
	@Get()
	@ApiUnauthorizedResponse()
	auth(@Request() req) {
		console.log('user: ', req.user);
		return;
	}
}
