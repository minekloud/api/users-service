import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../users/entities/user.entity';
import { UsersService } from '../users/users.service';
import { DeepPartial, Repository } from 'typeorm';
import { Credentials } from './entities/credentials.entity';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
	constructor(
		@InjectRepository(Credentials)
		private readonly credentialsRepository: Repository<Credentials>,
		private readonly usersService: UsersService,
		private readonly jwtService: JwtService,
	) {}

	async validateUser(username: string, password: string): Promise<User> {
		const user = await this.usersService.findOneByUsername(username);
		if (!user) {
			return null;
		}

		const credential = await this.credentialsRepository.findOne(user.id);
		if (!(await bcrypt.compare(password, credential.hashedPassword))) {
			return null;
		}

		return user;
	}

	async login(user: User) {
		const payload = { sub: user.id };
		return {
			access_token: this.jwtService.sign(payload),
			user: user,
		};
	}

	async register(
		partialUser: DeepPartial<User>,
		password: string,
	): Promise<User> {
		const user = await this.usersService.create(partialUser);

		const hashedPassword = await bcrypt.hash(password, 10);

		const credential = this.credentialsRepository.create({
			hashedPassword,
			user,
		});

		this.credentialsRepository.save(credential);

		return user;
	}
}
