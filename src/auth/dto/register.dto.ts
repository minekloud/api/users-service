import { IsEmail, IsNotEmpty, Length, Matches } from 'class-validator';

export class RegisterDto {
	@IsNotEmpty()
	@Length(3, 16)
	username: string;

	@IsEmail({ domain_specific_validation: true })
	email: string;

	@IsNotEmpty()
	@Matches(/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(\S){8,}$/, {
		message:
			'password must contain at least 1 lowercase character, ' +
			'1 uppercase character, 1 digit, and must be at least 8 characters long.',
	})
	password: string;
}
