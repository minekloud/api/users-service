import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import * as ormconfig from '../ormconfig';
import { UnleashModule } from './unleash/unleash.module';
import { Environment } from './config/environement.enum';
import { configuration } from './config/configuration';

@Module({
	imports: [
		ConfigModule.forRoot({
			load: [configuration],
			ignoreEnvFile: process.env.NODE_ENV == Environment.Production,
			isGlobal: true,
		}),
		UsersModule,
		TypeOrmModule.forRoot({
			...ormconfig,
			type: 'postgres',
			autoLoadEntities: true,
		}),
		AuthModule,
		UnleashModule.forRootAsync({
			global: true,
			imports: [ConfigModule],
			useFactory: async (configService: ConfigService) => ({
				url: configService.get('unleash.url'),
				instanceId: configService.get('unleash.instanceId'),
				appName: configService.get('unleash.appName'),
			}),
			inject: [ConfigService],
		}),
	],
	controllers: [],
	providers: [],
})
export class AppModule {}
