import { FeatureDisabledGuard } from './feature-disabled.guard';

describe('FeatureDisabledGuard', () => {
  it('should be defined', () => {
    expect(new FeatureDisabledGuard()).toBeDefined();
  });
});
