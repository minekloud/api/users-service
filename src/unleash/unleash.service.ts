import { Inject, Injectable } from '@nestjs/common';
import { Context, Unleash } from 'unleash-client';
import { UnleashModuleOptions } from './interfaces/unleash-module-options.interface';

@Injectable()
export class UnleashService {
	private instance: Unleash;

	constructor(
		@Inject('UNLEASH_OPTIONS') private options: UnleashModuleOptions,
	) {
		try {
			this.instance = new Unleash({
				url: options.url,
				appName: options.appName,
				instanceId: options.instanceId,
			});
		} catch (e) {
			console.log(e);
			
		}

		this.instance
			.on('error', console.error)
			.on('warn', console.warn)
			.on('ready', console.log)

	}

	public isEnabled(flag: string, context: Context = {}, defaultValue?) {
		return this.instance.isEnabled(flag, context, defaultValue);
	}
}
