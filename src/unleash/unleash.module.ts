import { DynamicModule, Module, Provider } from '@nestjs/common';
import {
	UnleashModuleAsyncOptions,
	UnleashModuleOptions,
	UnleashModuleOptionsFactory,
} from './interfaces/unleash-module-options.interface';
import { UnleashService } from './unleash.service';

@Module({})
export class UnleashModule {
	static forRoot(options: UnleashModuleOptions): DynamicModule {
		return {
			global: options.global || false,
			module: UnleashModule,
			providers: [
				{
					provide: 'UNLEASH_OPTIONS',
					useValue: options,
				},
				UnleashService,
			],
			exports: [UnleashService],
		};
	}

	static forRootAsync(options: UnleashModuleAsyncOptions): DynamicModule {
		return {
			global: options.global || false,
			module: UnleashModule,
			imports: options.imports || [],
			providers: this.createAsyncProviders(options),
			exports: [UnleashService],
		};
	}

	static createAsyncProviders(options: UnleashModuleAsyncOptions) {
		if (options.useExisting || options.useFactory) {
			return [UnleashService, this.createAsyncOptionsProvider(options)];
		}
		return [
			UnleashService,
			this.createAsyncOptionsProvider(options),
			{
				provide: options.useClass,
				useClass: options.useClass,
			},
		];
	}

	private static createAsyncOptionsProvider(
		options: UnleashModuleAsyncOptions,
	): Provider {
		if (options.useFactory) {
			return {
				provide: 'UNLEASH_OPTIONS',
				useFactory: options.useFactory,
				inject: options.inject || [],
			};
		}
		return {
			provide: 'UNLEASH_OPTIONS',
			useFactory: async (optionsFactory: UnleashModuleOptionsFactory) =>
				await optionsFactory.createUnleashOptions(),
			inject: [options.useExisting || options.useClass],
		};
	}
}
