import { FeatureEnabledGuard } from './feature-enabled.guard';

describe('FeatureEnabledGuard', () => {
  it('should be defined', () => {
    expect(new FeatureEnabledGuard()).toBeDefined();
  });
});
